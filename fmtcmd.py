import re, os, sys

with open('src/commands.go', 'r') as f:
    lines = []
    for line in f.readlines():
        #                          [0]: name             [1]: number
        registerDec = re.compile(r'const\s*(\w*)\s*command\s*=\s*(0x\w)')

        matches = re.search(registerDec, line)

        if matches is not None:
            newline = "{}: aurora.Colorize(\"{}\", commandColor),\n".format(
                matches.groups()[1],
                matches.groups()[0]
            )

            lines.append(newline)
    
    with open('src/formatmaps.go', 'a') as g:
        for line in lines:
            g.write(line)
