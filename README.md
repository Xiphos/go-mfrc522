<h1>Go-MFRC522</h1>

This is a golang package for using an MFRC522 module with the Raspberry Pi. it uses the [go-rpio](https://github.com/stianeikeland/go-rpio) library for its SPI interface and GPIO memory mapping.

<h2>Raspberry Pi</h2>
[BCM2835 Datasheet](https://www.raspberrypi.org/app/uploads/2012/02/BCM2835-ARM-Peripherals.pdf)

<h2>RC522</h2>
[Datasheet](https://www.nxp.com/docs/en/data-sheet/MFRC522.pdf)

<h2>ISO 14443</h2>
http://e2e.ti.com/cfs-file/__key/communityserver-discussions-components-files/667/2072.ISO_5F00_NFC-Standards-and-Specifications-Overview_5F00_2014.pdf  

http://sweet.ua.pt/andre.zuquete/Aulas/IRFID/11-12/docs/IRFID_avz_2.pdf  

https://www.nxp.com/docs/en/application-note/AN10834.pdf  

https://www.nxp.com/docs/en/application-note/AN10833.pdf  

[An interesting debate on related interfaces](https://www.secureidnews.com/news-item/is-the-debate-still-relevant-an-in-depth-look-at-iso-14443-and-its-competing-interface-types/)
