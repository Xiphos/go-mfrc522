#Converts the old C++ version of the registers header to a go-compatible one
import sys, os, re

newlines = []
with open("registers.go", 'r') as f:
    for line in f.readlines():
        m = re.findall(r'Register\s*(\w*)\s*=(.{4});', line)

        line = []
        for match in m:
            regName = match[0]
            regAddr = match[1]
            cReg = "const {} = Register({}); ".format(regName, regAddr)
            line.append(cReg)
        
        newlines.append(line)
f.close()

with open("registers.go", 'a') as f:
    f.write('\n')
    for line in newlines:
        f.write(" ".join(line) + '\n')

f.close()

##Now make the register names start with lower case to suppress exporting rules

with open("registers.go", 'r+') as f:
    for line in f.readlines():
        m = re.search('const\s\w*\s')