import re, os, sys

with open('src/registers.go', 'r') as f:
    lines = []
    for line in f.readlines():
        #                          [0]: name             [1]: number
        registerDec = re.compile(r'(\w*)\s*=\s*register\((\dx\w{2})\)')

        matches = re.search(registerDec, line)

        if matches is not None:
            newline = "{}: aurora.Colorize(\"{}\", regColor),\n".format(
                matches.groups()[1],
                matches.groups()[0]
            )

            lines.append(newline)
    
    with open('src/formatmaps.go', 'a') as g:
        for line in lines:
            g.write(line)
