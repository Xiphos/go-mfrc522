package main

/*
	Holds basic functionality tests
	for the device
*/

import (
	//"github.com/stianeikeland/go-rpio"
	"fmt"
	"testing"
	"github.com/logrusorgru/aurora"
)

//TestVersion - tests that the device sends back its software version
func TestVersion(t *testing.T) {
	//Print out the version
	if version, e := GetVersion(); e != nil {
		t.Errorf("Could not get a valid software version: got %v", version)
	} else {
		t.Logf("%v", version)
	}
}

//TestFIFO tests a basic Write/Read functionality for the FIFO buffer
func TestFIFO(t *testing.T) {
	//Make sure its empty
	FIFOFlush()

	//Write some data to the fifo reg
	var fifoData = []byte{
		1, 2, 4,
		8, 16, 32,
		64, 128,
	}

	t.Logf("Read FIFO level: %v\n", aurora.Red(FIFOLevel()))

	//Try to write to the FIFO reg
	if e := FIFOWrite(fifoData); e != nil {
		fmt.Printf("%v", e)
	}

	t.Logf("Read FIFO level: %v\n", aurora.Red(FIFOLevel()))

	//This also tests that FIFOLevel() returns the correct level
	fifoRead := FIFORead(FIFOLevel())

	for i := 0; i < len(fifoData); i++ {
		if fifoData[i] != fifoRead[i] {
			t.Errorf("Error in reading back from FIFO: %08b in original set does not match %0b from fifo",
				aurora.Cyan(fifoData[i]), aurora.Cyan(fifoRead[i]))
		}
	}
}

//ExampleVersion - prints out the software version
func ExampleVersion() {
	fmt.Printf("\n===== Software Version Example =====\n")

	//Print out the version
	if version, e := GetVersion(); e != nil {
		fmt.Printf("Could not get a valid software version: got %v", version)
	} else {
		fmt.Printf("%v\n", version)
		fmt.Println(aurora.Green(aurora.Bold("Successfully read the software version!")))
	}
}

//ExampleFIFO - shows a basic Write/Read functionality for the FIFO buffer
func ExampleFIFO() {
	//Example title
	fmt.Printf("\n===== FIFO Write/Read Example =====\n")

	//Flush the FIFO before we run the function just in case
	FIFOFlush()

	//Write some data to the fifo reg (nice numbers)
	var fifoData = []byte{
		0x01, 0x02, 0x04,
		0x08, 0x10, 0x20,
		0x40, 0x80,
	}

	preWriteLvl := FIFOLevel()
	fmt.Printf("Reading FIFO level: %v\n", aurora.Colorize(preWriteLvl, dataSizeColor))
	if preWriteLvl != 0 {
		defer fmt.Printf("ERROR: Flushed FIFO but read a level of %v instead of 0!",
			preWriteLvl)
	}

	//Try to write to the FIFO reg
	if e := FIFOWrite(fifoData); e != nil {
		fmt.Printf("%v", e)
	}

	postWriteLvl := FIFOLevel()
	fmt.Printf("Read FIFO level: %v\n", aurora.Colorize(postWriteLvl, dataSizeColor))
	if postWriteLvl-preWriteLvl != len(fifoData) {
		defer fmt.Printf("ERROR: FIFO level increased by %v instead of %v (length of data sent)",
			aurora.Colorize(postWriteLvl-preWriteLvl, dataSizeColor),
			aurora.Colorize(len(fifoData), dataSizeColor))
	}

	//This also tests that FIFOLevel() returns the correct level
	fifoRead := FIFORead(FIFOLevel())
	allCorrect := true
	for i := 0; i < len(fifoData); i++ {
		if fifoData[i] != fifoRead[i] {
			defer fmt.Printf("Error in reading back from FIFO: %v in original set does not match %v from fifo\n", HexRegData(fifoData[i]), HexRegData(fifoRead[i]))
			allCorrect = false
		}
	}

	//Final message before error printing (if any)
	if allCorrect {
		fmt.Println(aurora.Green(aurora.Bold("All values read from FIFO match what we sent!")))
	} else {
		fmt.Println(aurora.Red(aurora.Bold("Something went wrong in the example")))
	}
}




//ExampleIRQ - shows a basic usage of the IRQHandler interface
//			   makes a sample handler interface, then fills up the fifo
//			   the message that the FIFO IRQ returns should be printed around the time the FIFO gets filled
func ExampleIRQ() {
	var handler BasicIRQ
	StartIRQ(handler)

	//!Dont use the FIFOwrite function - we want to fill it up, and FIFOWrite prevents
	//!									filling up the FIFO buffer
	FIFOFlush()
	filler := make([]byte, fifoCap) 		//Make a buffer that will fill it
	WriteReg(fifoDataReg, filler)			//Write it to FIFO
	
	crc := CalculateCRC()
	fmt.Printf("%v", crc)

	//*A message should be printed
}




