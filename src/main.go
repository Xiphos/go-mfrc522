package main

/*
	Runs some of the testing functions and contains some convenient
	debugging functions
*/

import(
	"github.com/stianeikeland/go-rpio"
	"fmt"
	"strings"
	"github.com/logrusorgru/aurora"
	"github.com/jessevdk/go-flags"
	//"time"
)

func init(){
	//Try to initialize the GPIO
	if err := rpio.Open(); err != nil{
		panic(err)
	} else {
		fmt.Println("GPIO setup was a success")
	}

	//Set up the device
	InitDevice(rpio.Spi0, 5000000 , 0)	
}

func main() {
	defer rpio.SpiEnd(rpio.Spi0)

	/*                 PARSE FLAGS                  */
	var options struct{
		RunBasics bool     `short:"b" long:"run-basics" description:"Runs basic tests to check functionality"`
		RunTest   []string `short:"t" long:"test" description:"Runs the specified test, possible tests:\nversion\nfifo\nTo run multiple tests, use the flag multiple times, for example:\n-t version -t fifo\n"`
	}

	var testMap = map[string]func(){
		"version": ExampleVersion,
		"fifo":ExampleFIFO,
	}

	flags.Parse(&options)


	/*                 RUN FUNCTIONS                */
	
	//-b flag
	if options.RunBasics{
		ExampleVersion()
		ExampleFIFO()
	}

	//-t flag
	for _, test := range options.RunTest{
		//Call all the functions they specified with the t flag
		if testFunc := testMap[test]; testFunc != nil{
			testFunc()
		} else { 
			//They specified a function that was not recognized
			fmt.Printf("Unrecognized test function \"%v\"\n", options.RunTest)
		}
	}
}

//SliceAsHex - returns a string that formats a slice like [ 0xAA 0xFF 0x01 ]
func SliceAsHex(s []byte, c aurora.Color) string{
	var result string

	result = fmt.Sprint("[")
	
	for _, val:= range s{
		tmp := fmt.Sprintf(" 0x%02X ", val)
		result += fmt.Sprintf("%v", aurora.Colorize(tmp, c))
	}

	result += fmt.Sprintf("]")
	return result
}

//HexRegData - returns a string that formats register data like 0xFF with the datacolor
func HexRegData(s ...byte) string{
	var result string

	//Add in all the data we are sent
	for _, data := range s{
		result += fmt.Sprintf(" 0x%02X ", data)
	}

	result = strings.TrimSpace(result) 	//Remove the space before and after 
										//(but keep the inner ones)

	result = fmt.Sprintf("%v", aurora.Colorize(result, regDataColor))	//Colorize the whole thing
	return result
}