package main

import(
	"github.com/stianeikeland/go-rpio"
	"fmt"
	"errors"
	"github.com/logrusorgru/aurora"
)


//The possible values for the RFCCfgReg
type rxGain uint8
var rxGains = []rxGain{
	0x00 << 4,
	0x01 << 4,
	0x02 << 4,
	0x03 << 4,
	0x04 << 4,
	0x05 << 4,
	0x06 << 4,
	0x07 << 4,
}

//InitDevice - Initialise the device with the given pins
func InitDevice(spidevice rpio.SpiDev, speed int, chipsel uint8){

	//Initialise the spi interface
	rpio.SpiBegin(rpio.Spi0)
	rpio.SpiSpeed(speed)
	rpio.SpiChipSelect(chipsel)

	//These should be high
	rst.Write(rpio.High)
	ss.Write(rpio.High)
}


//GetVersion - Print the MFRC522 software version
func GetVersion() (string, error){

	//The possible software versions (9.3.4.8 in datasheet)
	mfrcVersions := map[byte]string {
		0x91: "MFRC522 version 1.0",
		0x92: "MFRC522 version 2.0",
	}

	//Read versionReg
	buffer := ReadReg(versionReg, 1)

	//Parse it
	var version string
	var valid 	bool
	if version, valid = mfrcVersions[buffer[0]]; !valid{
		e := fmt.Sprintf("Invalid MFRC version %v (check pins?)", HexRegData(buffer[0]))
		return HexRegData(buffer[0]) , errors.New(e)
	}


	return fmt.Sprintf("Version: %v [%v]\n", aurora.Cyan(aurora.Bold(version)), HexRegData(buffer[0])), nil
}

