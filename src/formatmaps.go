package main

/*
	Defines maps that map values to user-friendly strings,
	according to colors defined in colors.go
*/

import(
	"github.com/logrusorgru/aurora"
)

//RegStrMap - maps register addresses to colored names
var RegStrMap = map[register]aurora.Value{
	0x01: aurora.Colorize("CommandReg", 		regNameColor),
	0x02: aurora.Colorize("ComIEnReg", 			regNameColor),
	0x03: aurora.Colorize("DivIEnReg", 			regNameColor),
	0x04: aurora.Colorize("ComIrqReg", 			regNameColor),
	0x05: aurora.Colorize("DivIrqReg", 			regNameColor),
	0x06: aurora.Colorize("ErrorReg", 			regNameColor),
	0x07: aurora.Colorize("Status1Reg", 		regNameColor),
	0x08: aurora.Colorize("Status2Reg", 		regNameColor),
	0x09: aurora.Colorize("FIFODataReg", 		regNameColor),
	0x0A: aurora.Colorize("FIFOLevelReg", 		regNameColor),
	0x0B: aurora.Colorize("WaterLevelReg", 		regNameColor),
	0x0C: aurora.Colorize("ControlReg", 		regNameColor),
	0x0D: aurora.Colorize("BitFramingReg", 		regNameColor),
	0x0E: aurora.Colorize("CollReg", 			regNameColor),
	0x11: aurora.Colorize("ModeReg", 			regNameColor),
	0x12: aurora.Colorize("TxModeReg", 			regNameColor),
	0x13: aurora.Colorize("RxModeReg", 			regNameColor),
	0x14: aurora.Colorize("TxControlReg", 		regNameColor),
	0x15: aurora.Colorize("TxASKReg", 			regNameColor),
	0x16: aurora.Colorize("TxSelReg", 			regNameColor),
	0x17: aurora.Colorize("RxSelReg", 			regNameColor),
	0x18: aurora.Colorize("RxThresholdReg", 	regNameColor),
	0x19: aurora.Colorize("DemodReg", 			regNameColor),
	0x1C: aurora.Colorize("MfTxReg", 			regNameColor),
	0x1D: aurora.Colorize("MfRxReg", 			regNameColor),
	0x1F: aurora.Colorize("SerialSpeedReg", 	regNameColor),
	0x21: aurora.Colorize("CRCResultReg", 		regNameColor),
	0x22: aurora.Colorize("CRCResultReg2", 		regNameColor),
	0x24: aurora.Colorize("ModWidthReg", 		regNameColor),
	0x26: aurora.Colorize("RFCfgReg", 			regNameColor),
	0x27: aurora.Colorize("GSNReg", 			regNameColor),
	0x28: aurora.Colorize("CWGsPReg", 			regNameColor),
	0x29: aurora.Colorize("ModGsPReg", 			regNameColor),
	0x2A: aurora.Colorize("TModeReg", 			regNameColor),
	0x2B: aurora.Colorize("TPrescalerReg", 		regNameColor),
	0x2C: aurora.Colorize("TReloadReg", 		regNameColor),
	0x2D: aurora.Colorize("TReloadReg2", 		regNameColor),
	0x2E: aurora.Colorize("TCounterValReg", 	regNameColor),
	0x2F: aurora.Colorize("TCounterValReg2", 	regNameColor),
	0x31: aurora.Colorize("TestSel1Reg", 		regNameColor),
	0x32: aurora.Colorize("TestSel2Reg", 		regNameColor),
	0x33: aurora.Colorize("TestPinEnReg", 		regNameColor),
	0x34: aurora.Colorize("TestPinValueReg", 	regNameColor),
	0x35: aurora.Colorize("TestBusReg", 	 	regNameColor),
	0x36: aurora.Colorize("AutoTestReg", 	 	regNameColor),
	0x37: aurora.Colorize("VersionReg", 		regNameColor),
	0x38: aurora.Colorize("AnalogTestReg", 		regNameColor),
	0x39: aurora.Colorize("TestDAC1Reg", 		regNameColor),
	0x3A: aurora.Colorize("TestDAC2Reg", 		regNameColor),
	0x3B: aurora.Colorize("TestADCReg", 		regNameColor),
}

//CmdStrMap - maps command values to their names
var CmdStrMap = map[command]aurora.Value{
	0x00: aurora.Colorize("Idle", 		cmdNameColor),
	0x01: aurora.Colorize("Mem", 		cmdNameColor),
	0x02: aurora.Colorize("RandID", 	cmdNameColor),
	0x03: aurora.Colorize("CalcCRC", 	cmdNameColor),
	0x04: aurora.Colorize("Transmit", 	cmdNameColor),
	0x07: aurora.Colorize("NoCmdChg", 	cmdNameColor),
	0x08: aurora.Colorize("Receive", 	cmdNameColor),
	0x0C: aurora.Colorize("Transceive", cmdNameColor),
	0x0E: aurora.Colorize("MfAuthent", 	cmdNameColor),
	0x0F: aurora.Colorize("SoftReset", 	cmdNameColor),

	
}

//GainStrMap - maps gain values to names
var GainStrMap = map[rxGain]aurora.Value{
	0x00<<4: aurora.Colorize("18dB", gainNameColor),
	0x01<<4: aurora.Colorize("23dB", gainNameColor),
	0x02<<4: aurora.Colorize("18dB", gainNameColor),
	0x03<<4: aurora.Colorize("23dB", gainNameColor),
	0x04<<4: aurora.Colorize("33dB", gainNameColor),
	0x05<<4: aurora.Colorize("38dB", gainNameColor),
	0x06<<4: aurora.Colorize("43dB", gainNameColor),
	0x07<<4: aurora.Colorize("48dB", gainNameColor),
}
