package main

/*
	Defines the register type as well as the various registers
	defined in the datasheet. Names begin with a lower case to
	reflect go's exporting name rules
*/

import(
	"github.com/stianeikeland/go-rpio"
	"fmt"
	//"errors"
	"github.com/logrusorgru/aurora"
)

type register byte

//Various constants relating to registers


//The MSB for a read/write to a register
const(
	readhead    = byte(0x80)
	readtail    = byte(0x00) //The last byte in a read transaction
	writehead	= byte(0x00)
)

//Page 0
const(
	commandReg 		= register(0x01)
	comIEnReg 		= register(0x02)
	divIEnReg 		= register(0x03)
	comIrqReg 		= register(0x04)
	divIrqReg 		= register(0x05)
	errorReg 		= register(0x06)
	status1Reg 		= register(0x07)
	status2Reg 		= register(0x08)
	fifoDataReg 	= register(0x09)	//FIFO
	fifoLevelReg 	= register(0x0A)	//Registers
	waterLevelReg 	= register(0x0B)
	controlReg 		= register(0x0C)
	bitFramingReg 	= register(0x0D)
	collReg 		= register(0x0E)
)

//Page 1
const(
	modeReg 		= register(0x11)
	txModeReg 		= register(0x12)
	rxModeReg 		= register(0x13)
	txControlReg 	= register(0x14)
	txASKReg 		= register(0x15)
	txSelReg 		= register(0x16)
	rxSelReg 		= register(0x17)
	rxThresholdReg 	= register(0x18)
	demodReg 		= register(0x19)
	mfTxReg 		= register(0x1C)
	mfRxReg 		= register(0x1D)
	serialSpeedReg 	= register(0x1F)
)

//Page 2
const(
	crcResultReg 	= register(0x21)
	crcResultReg2 	= register(0x22)
	modWidthReg 	= register(0x24)
	rfCfgReg 		= register(0x26)
	gsNReg 			= register(0x27)
	cwgsPReg 		= register(0x28)
	modGsPReg 		= register(0x29)
	tModeReg 		= register(0x2A)	//////////////////////
	tPrescalerReg 	= register(0x2B)	//					//
	tReloadReg 		= register(0x2C)	//		Timing		//
	tReloadReg2 	= register(0x2D)	//	   Registers	//
	tCounterValReg 	= register(0x2E)	//				    //
	tCounterValReg2 = register(0x2F)	//////////////////////
)

//Page 3
const(
	testSel1Reg 	= register(0x31)
	testSel2Reg 	= register(0x32)
	testPinEnReg 	= register(0x33)
	testPinValueReg = register(0x34)
	testBusReg 		= register(0x35)
	autoTestReg 	= register(0x36)
	versionReg 		= register(0x37)	// Use for basic functionality test
	analogTestReg 	= register(0x38)
	testDAC1Reg 	= register(0x39)
	testDAC2Reg 	= register(0x3A)
	testADCReg 		= register(0x3B)
)

var regStrings = map[register]string{
	0x01: "command",
}


//FUNCTIONS

//ReadReg - Read a number of bytes from a register and return as a buffer
func ReadReg(regaddr register, bytes int) (buf []byte){

	//Initialise the buffer as per 8.1.2.1 in the datasheet
	var buffer 	 = make([]byte, bytes+1)
	addressByte := readhead | byte(regaddr << 1)

	//Bytes 0-n should have the address in them, and byte n+1 should have 00
	for i:= 0; i < bytes; i++{
		buffer[i] = addressByte
	}
	buffer[bytes] = readtail

	//Exchange the buffer with the device
	rpio.SpiExchange(buffer)

	//Debugging
	fmt.Printf("Read %v byte(s) from %v (%v): [%v]\n", 
				aurora.Colorize(len(buffer[1:]), dataSizeColor),
				RegStrMap[regaddr],
				HexRegData(byte(regaddr)),
				HexRegData(buffer[1:]...))

	return buffer[1:]
}

//WriteReg - Write a buffer to the register
func WriteReg(regaddr register, buffIn []byte){

	//Initialise the buffer and set the first byte to the 
	//read[register]0 address
	var buffer 		= make([]byte, len(buffIn) + 1)
		buffer[0]   = writehead | byte(regaddr << 1)

	//Copy over the contents we want to write
	for i:=1 ; i <= len(buffIn) ; i++ {
		buffer[i] = buffIn[i-1]
	}

	//Debugging
	fmt.Printf("Wrote %v byte(s) to %v (%v): [%v]\n", 
											   aurora.Colorize(len(buffIn), dataSizeColor),
											   RegStrMap[regaddr], 
											   HexRegData(byte(regaddr)),
											   HexRegData(buffer[1:]...))

	rpio.SpiTransmit(buffer...)
}
