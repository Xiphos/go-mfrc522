package main

/*
	Defines colors for various datatypes for debugging
*/

import(
	"github.com/logrusorgru/aurora"
)

const regDataColor = aurora.Color(aurora.MagentaFg | aurora.BoldFm) //Data read from a register
const regNameColor = aurora.Color(aurora.BlueFg    | aurora.BoldFm)	//The name of a register
const dataSizeColor= aurora.Color(aurora.RedFg)						//The size of some data
const cmdNameColor = aurora.Color(aurora.BrownFg | aurora.BoldFm)	//Name of a command
const gainNameColor= aurora.Color(aurora.BlueFg | aurora.BoldFm)	//RFC Gain values