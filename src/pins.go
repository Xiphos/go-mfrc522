package main

/*
	Defines the hardware pins to be used.
	uses the BCM numbering scheme. This should be
	configurable in the future
*/

import(
	"github.com/stianeikeland/go-rpio"
)

type pin int

const(
	sclk 	= rpio.Pin(11)
	mosi 	= rpio.Pin(10)
	miso 	= rpio.Pin(9)
	ss 		= rpio.Pin(8)
	irq 	= rpio.Pin(4)
	rst 	= rpio.Pin(25)
)
