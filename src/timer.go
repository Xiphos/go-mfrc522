package main

/*
	Defines various timer-related functions/values
	(see 10.3 in the datasheet)
*/

import(
	//"github.com/stianeikeland/go-rpio"
	//"fmt"
	//"errors"
	//"github.com/logrusorgru/aurora"
)

const(
	//TAuto - timer auto mode (table 106, 9.3.3.10)
	TAuto = byte(0x80)				//1.......

	//TGatedNone - internal timer does not run in gated mode
	TGatedNone = byte(0x00) 		//.00.....

	//TGatedMFIN - internal timer is gated by MFIN
	TGatedMFIN = byte(0x20) 		//.01.....

	//TGatedAUX1 - internal timer is gated by AUX1
	TGatedAUX1 = byte(0x40) 		//.10.....

	//TAutoRestart - timer automatically restarts once hitting 0
	TAutoRestart = byte(0x10) 		//...1....

	//TAutoRestartNone - timer generates an interrupt on TimerIRq when it hits 0
	TAutoRestartNone = byte(0x00) 	//........
)

//TimerValue - returns the current timer value (9.3.3.12)
func TimerValue() uint16{
	firstByte := ReadReg(tCounterValReg, 1)[0]
	secondByte := ReadReg(tCounterValReg2, 1)[0]

	total := (uint16(firstByte)<< 8) | uint16(secondByte) //The larger bits must be shifted by one byte

	return total
}

//TimerReload - returns the current timer reload value
func TimerReload() uint16{
	firstByte := ReadReg(tReloadReg, 1)[0]
	secondByte := ReadReg(tReloadReg2, 1)[0]

	total := (uint16(firstByte) << 8) | uint16(secondByte) //The larger bits must be shifted by one byte

	return total
}

//SetTimerReload - sets the current timer reload value
func SetTimerReload(set uint16){
	firstByte := byte(set >> 8)
	secondByte := byte(set & 0x0F)

	WriteReg(tReloadReg, []byte{firstByte})
	WriteReg(tReloadReg2, []byte{secondByte})
}

//TimerPrescaler - returns the current prescaler value
func TimerPrescaler() uint16{
	firstFour 	:= ReadReg(tModeReg, 1)[0] & 0x0F
	lowestEight := ReadReg(tPrescalerReg, 1)[0]
					// 0000 xxxx     +  xxxx xxxx (12 bit value)
	total := ((uint16(firstFour) << 8) | uint16(lowestEight))

	return total
}

//SetTimerPrescaler - set the current prescaler value
func SetTimerPrescaler(set uint16){
	modeHead := ReadReg(tModeReg, 1)[0] & 0xF0 //Select the first 4 mode bits
											   //xxxx 0000
	
	firstFour :=      (set & 0x0F00) >> 8 //Select the first four bits
	lastEight :=  byte(set & 0x00FF)      //Select the last eight bits

	newModeReg := modeHead | byte(firstFour)
	WriteReg(tModeReg,      []byte{newModeReg})
	WriteReg(tPrescalerReg, []byte{lastEight})
}