package main

/*
	Defines various power modes (8.6 in datasheet)
*/

import(
	"github.com/stianeikeland/go-rpio"
	"fmt"
	"github.com/logrusorgru/aurora"
)

//Constants
const powerDown   = 0x08 //0000 1000 (used to set  the powerDown bit)
const powerStatus = 0x08 //0000 1000 (used to read the powerDown bit)

//HardShutoff - Forces the shutoff of the device (8.6.1 in datasheet)
//				outputs are frozen
func HardShutoff(){
	rpio.WritePin(ss, rpio.Low)
}

//TurnOn - Bring back the device from hardshutoff mode
func TurnOn(){
	rpio.WritePin(ss, rpio.High)
}

//SoftShutoff - Enter the soft power-down mode (8.6.2 in datasheet)
//				NOTE: Blocks until shutoff is done!
func SoftShutoff(){
	regval := ReadReg(commandReg, 1)[0]

	regval |= powerDown //Set the PowerDown bit to 1

	WriteReg(commandReg, []byte{regval})

	//Keep looping until the device sets the bit to 0
	for status:= byte(1) ;; status = ReadReg(commandReg, 1)[0] & powerStatus{
		if status == 0{
			break
		}
	}

	fmt.Println(aurora.Red("Device has been shut off"))
}

//AntennaShutoff - Shut off the antennas (disable the RF field, 8.6.3 in datasheet)
func AntennaShutoff(){

}