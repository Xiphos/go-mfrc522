package main

/*
	Defines FIFO related functions/constants (8.3 in datasheet)
*/

import(
	//"github.com/stianeikeland/go-rpio"
	"fmt"
	"errors"
	//"github.com/logrusorgru/aurora"
)

//Fifo constants
const fifoCap		= 64		 //Byte capacity of FIFO
const fifoFlush		= byte(0x80) //Mask that will flush FIFO

//FIFOWrite - safely writes to fifo buffer, checking for capacity
func FIFOWrite(data []byte) (error){
	currentLevel := FIFOLevel()
	sizeToWrite  := len(data)

	//If we are going to overflow the FIFO, throw an error
	if (currentLevel + sizeToWrite) > fifoCap{
		err := fmt.Sprintf("Prevented FIFO Write: FIFO would overflow (%d bytes remaining)", fifoCap - currentLevel)
		return errors.New(err)
	}

	//Otherwise simply write to the FIFO register
	WriteReg(fifoDataReg, data)

	return nil
}

//FIFOLevel - returns the number of bytes in the FIFO buffer as int
func FIFOLevel() (int){
	level := ReadReg(fifoLevelReg, 1)[0]

	return int(level)
}

//FIFORead - returns a number of bytes from FIFO as a buffer
//			 reads as many as possible, if given too many bytes
func FIFORead(bytes int) ([]byte){
	toRead    := bytes
	fifoLevel := FIFOLevel()

	//If we're asked to read more bytes than FIFO has, read as many as we can
	if bytes > fifoLevel{
		toRead = fifoLevel
	}

	buffer := ReadReg(fifoDataReg, toRead)

	return buffer;
}

//FIFOFlush - flushes the fifo buffer by resetting its pointers
//			  *see 8.3 in the datasheet
func FIFOFlush(){
	reg := ReadReg(fifoLevelReg, 1)			//Read
	newByte := reg[0] | fifoFlush			//Set

	//Debugging
	fmt.Printf("Sent %v to flush FIFO\n", HexRegData(newByte))

	WriteReg(fifoLevelReg, []byte{newByte})	//Write
}