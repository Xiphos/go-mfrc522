package main

/*
	Defines the commands for the commandReg
	(see 10.3 in the datasheet)
*/

import(
	//"github.com/stianeikeland/go-rpio"
	"fmt"
	//"errors"
	"time"
	"github.com/logrusorgru/aurora"
)

//Command - represents a 4-bit command executed by the device
type command uint8

//? Should these commands be exposed?
const(
	idle 	  	command = 0x0
	mem 	  	command = 0x1
	randID  	command = 0x2
	calcCRC 	command = 0x3
	transmit 	command = 0x4
	noCmdChg 	command = 0x7
	receive 	command = 0x8
	transceive 	command = 0xC
	mfAuthent 	command = 0xE
	softReset 	command = 0xF
)

//SendCmd - Send a command to the commandReg
func SendCmd(cmd command){
	currentState := ReadReg(commandReg, 1)[0]

	currentState &= 0xF0 //0b1111 0000, set the last 4 bits to 0
	currentState |= byte(cmd)

	WriteReg(commandReg, []byte{currentState})

	fmt.Printf("Sent %v command to device", CmdStrMap[cmd])
}

//GetCmd - returns the current command being executed by the device as a command code
//		   this is the internal version of the function, use GetCmdName for extensions
func getCmd() command{
	currentState := ReadReg(commandReg, 1)[0]

	currentState &= 0x0F //0b0000 1111, just get the last 4 bits

	return command(currentState)
}

//GetCmdName - returns the current command being executed by the device as a colored string
//			   doesn't really do much but access the appropriate map, but helps readability
func GetCmdName() aurora.Value{
	return CmdStrMap[getCmd()]
}

//CalculateCRC - sends a calcCRC command to CmdReg and returns the result
//				 blocks input
func CalculateCRC() uint16{
	SendCmd(calcCRC)
	
	//Keep checking the last 3 bits until its idle
	for ok := (ReadReg(commandReg, 1)[0] | 0x08); ok != byte(idle);ok = (ReadReg(commandReg, 1)[0] | 0x08){
		time.Sleep(time.Millisecond * 50) //Wait 50ms
	}

	//We have exited the loop, meaning the command was completed
	byte1 	:= uint16(ReadReg(crcResultReg, 1)[0])
	byte2 	:= uint16(ReadReg(crcResultReg2, 1)[0])
	CRC 	:= (byte1 << 8) | byte2

	return CRC
}