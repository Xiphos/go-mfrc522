#Build command for cross compiling
BUILDCMD=env GOOS=linux GOARCH=arm GOARM=6 go build
PROJNAME=mfrc522
SOURCES=src/*.go
BUILDPATH=bin/${PROJNAME}
FLAGS=-o ${BUILDPATH}

sendpi: compile ${SOURCES}
	scp ${BUILDPATH} pi@raspi0:

sendece: compile ${SOURCES}
	scp ${BUILDPATH} s5khatta@ecelinux4.uwaterloo.ca:mfrc522 ##Put it in ecelinux, run getrc on pi to receive

compile:
	${BUILDCMD} ${FLAGS} ${SOURCES}
